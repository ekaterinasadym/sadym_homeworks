import java.util.Arrays;

public class Main {
    public static int findIndex(int[] arrayOfInts, int b) {
        int i = 0;
        while (i < arrayOfInts.length) {
            int a = arrayOfInts[i];
            if (a == b) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static void moveInts(int[] array) {
        int i = 0;
        while (i < array.length) {
            int a = array[i];
            if (a == 0) {
                int k = i;
                while (k < array.length - 1) {
                    array[k] = array[k + 1];
                    k++;
                }
                array[array.length - 1] = 0;
            }
            i++;
        }
    }

    public static void main(String[] args) {
        int[] inputArray = {4, 0, 98, 2, 0, 96, -1};
        int index = findIndex(inputArray, 4);
        System.out.println("Result of findIndex: " + index);

        moveInts(inputArray);
        System.out.println("Result of moveInts: " + Arrays.toString(inputArray));
    }
}