package homework07;

import java.util.Scanner;

public class Homework07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int[] counters = new int[201];
        while (a != -1) {
            counters[a + 100] += 1;
            a = scanner.nextInt();
        }
        int lowestCount = 2_147_483_647;
        int result = -1;
        for (int i = -100; i <= 100; i++) {
            int count = counters[i + 100];
            if (count == 0) {
                continue;
            }
            if (count < lowestCount) {
                lowestCount = count;
                result = i;
            }
        }
        System.out.println(result);
    }
}
