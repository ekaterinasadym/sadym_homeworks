package homework08;

import java.util.Arrays;
import java.util.Comparator;

public class Homework08 {
    public static void main(String[] args) {
        Human[] people = {
                new Human("Alex", 80),
                new Human("Kate", 55),
                new Human("Maks", 75),
                new Human("Anna", 60),
                new Human("Zhirushka", 99),
                new Human("Malysh", 30),
                new Human("Oleg", 87),
                new Human("Jessika", 46),
                new Human("Zhirini", 124),
                new Human("Nastya", 32)
        };
        Arrays.sort(people, Comparator.comparing(Human::getWeight));
        System.out.println(Arrays.toString(people));
    }
}