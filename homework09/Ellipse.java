package homework09;

public class Ellipse extends Figure {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    public int getPerimeter() {
        return (int) (4 * ((Math.PI * x * y + Math.pow((x - y), 2)) / (x + y)));
    }
}
