package homework09;

public class Main {
    public static void main (String[] args) {
        Rectangle rectangle = new Rectangle(20, 15);
        Ellipse ellipse = new Ellipse(20, 15);
        Square square = new Square(20);
        Circle circle = new Circle(20);

        System.out.println(rectangle.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());
    }
}
