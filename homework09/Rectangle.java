package homework09;

public class Rectangle extends Figure{
    public Rectangle(int x, int y) {
        super(x, y);
    }

    public int getPerimeter() {
        return (x + y) * 2;

    }
}
