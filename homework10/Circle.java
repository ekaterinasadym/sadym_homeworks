package homework10;

public class Circle extends Ellipse implements MovableFigure {
    private int x = 0;
    private int y = 0;

    public Circle(int radius) {
        super(radius, radius);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                ", radius=" + a +
                '}';
    }
}
