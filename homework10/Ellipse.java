package homework10;

public class Ellipse extends Figure {
    public Ellipse(int a, int b) {
        super(a, b);
    }

    public int getPerimeter() {
        return (int) (4 * ((Math.PI * a * b + Math.pow((a - b), 2)) / (a + b)));
    }
}
