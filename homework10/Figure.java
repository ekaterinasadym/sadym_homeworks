package homework10;

public abstract class Figure {
    protected int a;
    protected int b;

    public Figure(int a, int b){
        this.a =a;
        this.b=b;
    }


    abstract int getPerimeter();
}
