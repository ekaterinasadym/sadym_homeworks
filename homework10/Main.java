package homework10;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MovableFigure[] movableFigures = {
                new Circle(5),
                new Square(4)
        };

        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < movableFigures.length; i++) {
            movableFigures[i].move(2, 3);
        }

        System.out.println(Arrays.toString(movableFigures));
    }

}
