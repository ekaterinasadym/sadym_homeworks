package homework10;

public interface MovableFigure {
    void move(int x, int y);
}
