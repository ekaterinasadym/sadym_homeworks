package homework10;

public class Rectangle extends Figure{
    public Rectangle(int a, int b) {
        super(a, b);
    }

    public int getPerimeter() {
        return (a + b) * 2;

    }
}
