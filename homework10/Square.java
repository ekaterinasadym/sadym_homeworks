package homework10;

public class Square extends Rectangle implements MovableFigure {
    private int x;
    private int y;

    public Square(int side) {
        super(side, side);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Square{" +
                "side=" + a +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}