package homework13;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 5, 6, 7, 9, 8, 14, 22, 54, 33};

        int[] result = Sequence.filter(array, number -> number % 2 == 0);
        System.out.println(Arrays.toString(result));

        int[] result2 = Sequence.filter(array, number -> {
            int a = number;
            int sum = 0;
            while (a != 0) {
                sum += a % 10;
                a /= 10;
            }
            return sum % 2 == 0;
        });
        System.out.println(Arrays.toString(result2));

    }
}
