package homework13;

import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition){

        int[] result = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            if (condition.isOk(value)) {
                result[j] = value;
                j++;
            }
        }
        int[] finalResult = new int[j];
        for (int i = 0; i < j; i++) {
            finalResult[i] = result[i];
        }
        return finalResult;
	}
}
