import java.util.Scanner;

class Homework05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int minDigit = 9;
        while (a != -1) {
            while (a != 0) {
            int lastDigit = a % 10;
                if (lastDigit < minDigit) {
                    minDigit=lastDigit;
                }
                a /= 10;
            }
            a = scanner.nextInt();
        }
        System.out.println("Result - " +minDigit);
    }
}